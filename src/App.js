import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {  
  const [todos, setTodos] = React.useState([])
  const [todo, setTodo] = React.useState([])

  function handelSubmit(e){
    e.preventDefault()

    const newTodo = {
      id: new Date().getTime(),
      text: todo,
      complated: false
    }

    setTodos([...todos].concat(newTodo))
    setTodo("")
  }

  function deleteTodo(id){
    const updatedTodos = [...todos].filter((todo) => todo.id !== id)
    setTodos(updatedTodos)
  }

  // function toggleComplate(id){
  //   const updatedTodos = [...todos].map((todo) => {
  //     if (todo.id === id) {
  //       todo.complated = !todo.complated
  //     }
  //     return todo
  //   })      

  //   setTodos(updatedTodos)
  // }

  return (
    <div style={{textAlign: 'center'}}>
      <form onSubmit={handelSubmit}>
        <h1>Todo List App</h1>
        <input className='rounded-input' type="text" onChange={(e) => setTodo(e.target.value)} value={todo}/>
        <button type="submit" className='btn-hire'>Add Todo</button>                
      </form>      
      <div>
      {todos.map((todo) => <div className='padding-top' key={todo.id}>
        {todo.text} 
        <button className='btn-hire' onClick={()=> deleteTodo(todo.id)}>Delete</button>                
        </div>)}
      </div>
      
    </div>
  );
}

export default App;
